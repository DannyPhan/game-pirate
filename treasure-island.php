<!DOCTYPE html>
<html lang="en">

<head>
    <title>Treasure Island</title>
    <!-- head -->
    <?php include "html/head.html";?>
</head>

<body id="top" class="shop-page treasure_page">
    <div class="treasure-page main-box">
        <img src="image/background/Sea.png" alt="" class="back-ground-main" />

        <!-- Header -->
        <?php include "html/header.html";?>

        <section class="container-fluid main-box treasuare-page" >
           <div class="row top">
           <aside class="menu-right">
                <div class="box ">
                    <img src="image/Button_cancel.png" alt="" class="background-icon">
                    <p class="text">1 CREW: 10 $PKT</p>
                </div>
            </aside>
               <div class="col top-block">
                <div class="count-down-block">
                    <div class="time-count-down">
                        <img src="image/timeless.png" alt="" class="background-img">
                        <div id="clockdiv">
                              <span class="hours time"></span>
                              <span class="time">:</span>
                              <span class="minutes time"></span>
                              <span class="time">:</span>
                              <span class="seconds time"></span>
                              <p class="text">Left</p>
                        </div>
                        <div class="days">13/01/2022</div>
                    </div>
                    <div class="message-count-down">
                        <img src="image/timeless-1.png"  class=" background-img" alt="">
                        <p class="text">Total hired crews:1000</p>
                    </div>
                </div>
               </div>
           </div>
           <div class="row">
            <aside class="menu-left">
                <div class="content-block">
                    <div class="cup-block one-block top-player">
                        <img src="image/treasure/icon_topplayer.png" alt="" class="background">
                        <img src="image/treasure/icon_top_player_active.png" alt="" class="background icon-active">
                        <p class="text">Top player</p>
                    </div>
                    <div class="chest-block one-block treasure-hunted">
                        <img src="image/treasure/icon_tresure_island_active.png" alt="" class="background">
                        <img src="image/treasure/icon_tresure_island.png" alt="" class="background icon-active">
                        <p class="text">Tresure Hunted</p>
                    </div>
                    <div class="book-block one-block rules">
                        <img src="image/treasure/icon_rules.png" alt="" class="background">
                        <img src="image/treasure/icon_rules_active.png" alt="" class="background icon-active">
                        <p class="text">Rule</p>
                    </div>
                    <div class="even-block one-block even">
                        <img src="image/treasure/icon_event.png" alt="" class="background">
                        <img src="image/treasure/icon_event_active.png" alt="" class="background icon-active">
                        <p class="text">Event</p>
                    </div>
                </div>
            </aside>
               <div class="col block-box">
                <div class="first-island island-block">
                    <div class=" first">
                        <div class="island-image win-ability">
                            <img src="image/treasure/Light_land_3.png" alt="">
                        </div>

                        <div class="island-content-box">
                            <img src="image/Popup_claimpkt.png" alt="" class="background-image">
                            <div class="title">
                                <img src="image/Treasure-Hunted.png" alt="" class="background-image">
                                <p class="text">Lethe Island</p>
                            </div>
                            <div class="quanlity-box">
                                <p class="text">Min 20</p>
                                <div class="number">
                                    <span class="minus">-</span>
                                    <input type="text" value="200"/>
                                    <span class="plus">+</span>
                                </div>
                                <p class="text">Max 200</p>
                            </div>

                            <div class="info-box">
                                <p class="text">Your Crews: 5</p>
                            </div>
                            
                            <div class="submit-block">
                                <img src="image/treasure/Button_cancel_treasure.png" alt="" class="background-icon">
                                <button type="submit" class="btn btn-primary"> Rent Crew</button>
                            </div>

                            <p class="text">Total crews on this Island: 100</p>
                        </div>
                    </div>
                </div>
                <div class=" second-island island-block">
                        <div class=" second">
                            <div class="island-image win-ability">
                                <img src="image/treasure/Light_land_1.png" alt="">
                            </div>

                            <div class="island-content-box">
                                <img src="image/Popup_claimpkt.png" alt="" class="background-image">
                                <div class="title">
                                    <img src="image/Treasure-Hunted.png" alt="" class="background-image">
                                    <p class="text">STYX Island</p>
                                </div>
                                <div class="quanlity-box">
                                    <p class="text">Min 20</p>
                                    <div class="number">
                                        <span class="minus">-</span>
                                        <input type="text" value="200"/>
                                        <span class="plus">+</span>
                                    </div>
                                    <p class="text">Max 200</p>
                                </div>

                                <div class="info-box">
                                    <p class="text">Your crews: 5</p>
                                </div>
                                
                                <div class="submit-block">
                                    <img src="image/treasure/Button_cancel_treasure.png" alt="" class="background-icon">
                                    <button type="submit" class="btn btn-primary">Hire Crew</button>
                                </div>

                                <p class="text">Total crews on this Island: 100</p>

                            </div>
                        </div>
                </div>
               </div>
               <div class="col block-box">
                <div class="third-island island-block">
                        <div class=" third">
                            <div class="island-image win-ability">
                                <img src="image/treasure/Light_land_2.png" alt="">
                            </div>

                            <div class="island-content-box">
                                <img src="image/Popup_claimpkt.png" alt="" class="background-image">
                                <div class="title">
                                    <img src="image/Treasure-Hunted.png" alt="" class="background-image">
                                    <p class="text">Acheron Island</p>
                                </div>
                                <div class="quanlity-box">
                                    <p class="text">Min 20</p>
                                    <div class="number">
                                        <span class="minus">-</span>
                                        <input type="text" value="200"/>
                                        <span class="plus">+</span>
                                    </div>
                                    <p class="text">Max 200</p>
                                </div>

                                <div class="info-box">
                                    <p class="text">Your crews: 5</p>
                                </div>
                                
                                <div class="submit-block">
                                    <img src="image/treasure/Button_cancel_treasure.png" alt="" class="background-icon">
                                    <button type="submit" class="btn btn-primary">Hire Crew</button>
                                </div>

                                <p class="text">Total crews on this Island: 100</p>

                            </div>
                        </div>
                </div>
                <div class="fourth-island island-block">
                    <div class=" fourth">
                            <div class="island-image win-ability">
                                <img src="image/treasure/Light_land_4.png" alt="">
                            </div>

                            <div class="island-content-box">
                                <img src="image/Popup_claimpkt.png" alt="" class="background-image">
                                <div class="title">
                                    <img src="image/Treasure-Hunted.png" alt="" class="background-image">
                                    <p class="text">Cocytus Island</p>
                                </div>
                                <div class="quanlity-box">
                                    <p class="text">Min 20</p>
                                    <div class="number">
                                        <span class="minus">-</span>
                                        <input type="text" value="200"/>
                                        <span class="plus">+</span>
                                    </div>
                                    <p class="text">Max 200</p>
                                </div>

                                <div class="info-box">
                                    <p class="text">Your crews: 5</p>
                                </div>
                                
                                <div class="submit-block">
                                    <img src="image/treasure/Button_cancel_treasure.png" alt="" class="background-icon">
                                    <button type="submit" class="btn btn-primary"> Hire Crew</button>
                                </div>

                                <p class="text">Total crews on this Island: 100</p>

                            </div>
                        </div>  
                </div>
               </div>


           </div>
        </section>

        <div class="popup-treasure">

            <div class="popup rules-popup popup-update" >
                <div class="popup-content">
                    <div class="close-modal">
                        <img src="image/Icon_close.png" alt="" class="background-image">
                    </div>
                    <div class="popup-body">
                        <img src="image/treasure/BG-rules.png" alt="" class="background-image">
                        <div class="title-popup">
                            <img src="image/Treasure-Hunted.png" alt="" class="background-image">
                            <p class="text">Alert</p>
                        </div>
                        <div class="main-box">
                            <div class="container-box">
                                <img src="image/treasure/BG_rules.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="popup even-popup popup-update custom-popup-two" >
                <div class="popup-content">
                    <div class="close-modal">
                        <img src="image/Icon_close.png" alt="" class="background-image">
                    </div>
                    <div class="popup-body">
                        <img src="image/Popup_claimpkt.png" alt="" class="background-image">
                        <div class="title-popup">
                            <img src="image/Treasure-Hunted.png" alt="" class="background-image">
                            <p class="text">Event</p>
                        </div>
                        <div class="main-box">
                            <div class="top-block">
                                <div class="text">Comming soon</div>
                            </div>
                            <div class="bottom-block">
                                <div class="submit-block right-block ">
                                    <img src="image/Button_cancel.png" alt="" class="background-icon">
                                    <button type="submit" class="btn btn-primary close-modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="popup win-ability-popup popup-update custom-popup-two" >
                <div class="popup-content">
                    <div class="close-modal">
                        <img src="image/Icon_close.png" alt="" class="background-image">
                    </div>
                    <div class="popup-body">
                        <img src="image/Popup_claimpkt.png" alt="" class="background-image">
                        <div class="title-popup">
                            <img src="image/Treasure-Hunted.png" alt="" class="background-image">
                            <p class="text">Win Ability</p>
                        </div>
                        <div class="main-box">
                            <div class="top-block">
                                <p class="text">10 crew(s) = 206 PKT</p>
                                <div class="text main">
                                    <img src="image/coin_pkt.png" alt="">
                                    <p>Win = 3000 PKT</p>
                                </div>
                                <div class="text sub-text">
                                    <p>1 BUSD = 2 PKT</p>
                                </div>
                            </div>
                            <div class="bottom-block">
                                <div class="submit-block right-block ">
                                    <img src="image/Button_cancel.png" alt="" class="background-icon">
                                    <button type="submit" class="btn btn-primary close-modal">Ok</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="popup treasure-hunted-popup treasure-full-width popup-update " >
                <div class="popup-content">
                    <div class="close-modal">
                        <img src="image/Icon_close.png" alt="" class="background-image">
                    </div>
                    <div class="popup-body">
                        <img src="image/treasure/background_treasure_large.png" alt="" class="background-image">
                        <div class="title-popup">
                            <img src="image/Treasure-Hunted.png" alt="" class="background-image">
                            <p class="text">Treasure Hunted</p>
                        </div>
                        <div class="main-box">
                            <div class="content-main">
                                <div class="top-content flex">
                                    <div class="left-content flex">
                                        <div class="date-time">
                                            <p>Jan 14/2022</p>
                                        </div>
                                        <div class="image-icon acheron">
                                            <img src="image/popup/popup-treasure-hunted/cocytus-island.png" alt="">
                                        </div>
                                    </div>

                                    <div class="right-content flex">
                                        <div class="pkt-block">
                                            <img src="image/popup/popup-treasure-hunted/2010-PKT.png" alt="">
                                            <p>+ 2010 pkt</p>
                                        </div>
                                        <div class="image-icon claim">
                                            <img src="image/popup/popup-treasure-hunted/Claimed.png" alt="">
                                        </div>
                                        <select name="service" class="dropdown-custom service">
                                            <option data-list="all"></option>
                                            <option data-list="Deco">Deco</option>
                                            <option data-list="Design">Design</option>
                                            <option data-list="Construction">Construction</option>
                                            <option data-list="Lighting">Lighting</option>
                                        </select>
                                    </div>

                                </div>

                                <div class="container-content">
                                    <ul class="item flex">
                                        <li class="item flex">
                                            <div class="island-name block-item">
                                                <div class="text-name content-w-box">
                                                        <img src="image/popup/popup-treasure-hunted/Island-1.png" alt="">

                                                    <p class="text">Styx Island</p>
                                                </div>
                                            </div>

                                            <div class="date-time flex block-item">
                                                <p class="time">12:00 PM</p>
                                            </div>
                                            
                                            <div class="pkt-block flex block-item">
                                                <div class="text">Deposited</div>
                                                <div class="number-pkt content-w-box">
                                                        <img src="image/popup/popup-treasure-hunted/2010-PKT.png" alt="">
                                                    <p class="number">110 PKT</p>
                                                </div>
                                            </div>

                                            <div class="info-battle block-item">
                                                <div class="text">Lose</div>
                                                <div class="number-pkt content-w-box">
                                                        <img src="image/popup/popup-treasure-hunted/2010-PKT.png" alt="">
                                                    <p class="number minus">- 110 PKT</p>
                                                </div>
                                            </div>

                                            <div class="info-battle block-item">
                                                <div class="text">Profit</div>
                                                <div class="number-pkt content-w-box">
                                                        <img src="image/popup/popup-treasure-hunted/2010-PKT.png" alt="">
                                                    <p class="number">0%</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item flex">
                                            <div class="island-name block-item">
                                                <div class="text-name content-w-box">
                                                        <img src="image/popup/popup-treasure-hunted/Island-1.png" alt="">

                                                    <p class="text">Styx Island</p>
                                                </div>
                                            </div>

                                            <div class="date-time flex block-item">
                                                <p class="time">12:00 PM</p>
                                            </div>
                                            
                                            <div class="pkt-block flex block-item">
                                                <div class="text">Deposited</div>
                                                <div class="number-pkt content-w-box">
                                                        <img src="image/popup/popup-treasure-hunted/2010-PKT.png" alt="">
                                                    <p class="number ">110 PKT</p>
                                                </div>
                                            </div>

                                            <div class="info-battle block-item">
                                                <div class="text">Lose</div>
                                                <div class="number-pkt content-w-box">
                                                        <img src="image/popup/popup-treasure-hunted/2010-PKT.png" alt="">
                                                    <p class="number plus">+ 110 PKT</p>
                                                </div>
                                            </div>

                                            <div class="info-battle block-item">
                                                <div class="text">Profit</div>
                                                <div class="number-pkt content-w-box">
                                                        <img src="image/popup/popup-treasure-hunted/2010-PKT.png" alt="">
                                                    <p class="number">0%</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item flex">
                                            <div class="island-name block-item">
                                                <div class="text-name content-w-box">
                                                        <img src="image/popup/popup-treasure-hunted/Island-1.png" alt="">

                                                    <p class="text">Styx Island</p>
                                                </div>
                                            </div>

                                            <div class="date-time flex block-item">
                                                <p class="time">12:00 PM</p>
                                            </div>
                                            
                                            <div class="pkt-block flex block-item">
                                                <div class="text">Deposited</div>
                                                <div class="number-pkt content-w-box">
                                                        <img src="image/popup/popup-treasure-hunted/2010-PKT.png" alt="">
                                                    <p class="number">110 PKT</p>
                                                </div>
                                            </div>

                                            <div class="info-battle block-item">
                                                <div class="text">Lose</div>
                                                <div class="number-pkt content-w-box">
                                                        <img src="image/popup/popup-treasure-hunted/2010-PKT.png" alt="">
                                                    <p class="number">- 110 PKT</p>
                                                </div>
                                            </div>

                                            <div class="info-battle block-item">
                                                <div class="text">Profit</div>
                                                <div class="number-pkt content-w-box">
                                                        <img src="image/popup/popup-treasure-hunted/2010-PKT.png" alt="">
                                                    <p class="number">0%</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item flex">
                                            <div class="island-name block-item">
                                                <div class="text-name content-w-box">
                                                        <img src="image/popup/popup-treasure-hunted/Island-1.png" alt="">

                                                    <p class="text">Styx Island</p>
                                                </div>
                                            </div>

                                            <div class="date-time flex block-item">
                                                <p class="time">12:00 PM</p>
                                            </div>
                                            
                                            <div class="pkt-block flex block-item">
                                                <div class="text">Deposited</div>
                                                <div class="number-pkt content-w-box">
                                                        <img src="image/popup/popup-treasure-hunted/2010-PKT.png" alt="">
                                                    <p class="number">110 PKT</p>
                                                </div>
                                            </div>

                                            <div class="info-battle block-item">
                                                <div class="text">Lose</div>
                                                <div class="number-pkt content-w-box">
                                                        <img src="image/popup/popup-treasure-hunted/2010-PKT.png" alt="">
                                                    <p class="number">- 110 PKT</p>
                                                </div>
                                            </div>

                                            <div class="info-battle block-item">
                                                <div class="text">Profit</div>
                                                <div class="number-pkt content-w-box">
                                                        <img src="image/popup/popup-treasure-hunted/2010-PKT.png" alt="">
                                                    <p class="number">0%</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="popup top-player-popup treasure-full-width popup-update" >
                <div class="popup-content">
                    <div class="close-modal">
                        <img src="image/Icon_close.png" alt="" class="background-image">
                    </div>
                    <div class="popup-body">
                        <img src="image/treasure/background_treasure_large.png" alt="" class="background-image">
                        <div class="title-popup">
                            <img src="image/Treasure-Hunted.png" alt="" class="background-image">
                            <p class="text">Top Player</p>
                        </div>
                        <div class="main-box">
                            <div class="content-main">
                                <div class="top-content flex">
                                    <div class="input-group">
                                        <div class="form-outline">
                                            <input type="search" id="form1" class="form-control" />
                                        </div>
                                        <button type="button" class="btn btn-primary">
                                            <img src="image/treasure/search-icon.png" alt="">
                                        </button>
                                    </div>
                                    <select name="service" class="service">
                                        <option data-list="all"></option>
                                        <option data-list="Deco">Deco</option>
                                        <option data-list="Design">Design</option>
                                        <option data-list="Construction">Construction</option>
                                        <option data-list="Lighting">Lighting</option>
                                    </select>
                                </div>

                                <div class="container-content">
                                    <div class="top-container">
                                        <ul class="items">
                                            <li class="item"><p class="text">Ranking</p></li>
                                            <li class="item"><p class="text">Contact</p></li>
                                            <li class="item"><p class="text">Rewarded</p></li>
                                            <li class="item"><p class="text">Win rate</p></li>
                                        </ul>
                                    </div>
                                    <div class="main-container">
                                        <ul class="items">
                                            <li class="item">
                                                <p class="text hidden-lg">Ranking</p>
                                                <img src="image/cup_10.png" alt="" class="right">
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Contact</p>
                                                <div class="box right">
                                                    <p class="text">XXXX XXXX XXXX</p>
                                                    <img src="image/treasure/contact-icon.png" alt="">
                                                </div>
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Rewarded</p>
                                                <p class="text right">10.000 PKT</p>
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Win rate</p>
                                                <p class="text right">80%</p>
                                            </li>
                                        </ul>
                                        <ul class="items">
                                            <li class="item">
                                                <p class="text hidden-lg">Ranking</p>
                                                <img src="image/treasure/rank-2.png" alt="" class="right">
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Contact</p>
                                                <div class="box right">
                                                    <p class="text">XXXX XXXX XXXX</p>
                                                    <img src="image/treasure/contact-icon.png" alt="">
                                                </div>
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Rewarded</p>
                                                <p class="text right">10.000 PKT</p>
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Win rate</p>
                                                <p class="text right">80%</p>
                                            </li>
                                        </ul>
                                        <ul class="items">
                                            <li class="item">
                                                <p class="text hidden-lg">Ranking</p>
                                                <img src="image/treasure/rank-3.png" alt="" class="right">
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Contact</p>
                                                <div class="box right">
                                                    <p class="text">XXXX XXXX XXXX</p>
                                                    <img src="image/treasure/contact-icon.png" alt="">
                                                </div>
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Rewarded</p>
                                                <p class="text right">10.000 PKT</p>
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Win rate</p>
                                                <p class="text right">80%</p>
                                            </li>
                                        </ul>
                                        <ul class="items">
                                            <li class="item">
                                                <p class="text hidden-lg">Ranking</p>
                                                <p class="number right">3</p>
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Contact</p>
                                                <div class="box right">
                                                    <p class="text">XXXX XXXX XXXX</p>
                                                    <img src="image/treasure/contact-icon.png" alt="">
                                                </div>
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Rewarded</p>
                                                <p class="text right">10.000 PKT</p>
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Win rate</p>
                                                <p class="text right">80%</p>
                                            </li>
                                        </ul>
                                        <ul class="items">
                                            <li class="item">
                                                <p class="text hidden-lg">Ranking</p>
                                                <p class="number right">5</p>
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Contact</p>
                                                <div class="box right">
                                                    <p class="text">XXXX XXXX XXXX</p>
                                                    <img src="image/treasure/contact-icon.png" alt="">
                                                </div>
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Rewarded</p>
                                                <p class="text right">10.000 PKT</p>
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Win rate</p>
                                                <p class="text right">80%</p>
                                            </li>
                                        </ul>
                                        <ul class="items">
                                            <li class="item">
                                                <p class="text hidden-lg">Ranking</p>
                                                <p class="number right">6</p>
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Contact</p>
                                                <div class="box right">
                                                    <p class="text">XXXX XXXX XXXX</p>
                                                    <img src="image/treasure/contact-icon.png" alt="">
                                                </div>
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Rewarded</p>
                                                <p class="text right">10.000 PKT</p>
                                            </li>
                                            <li class="item">
                                                <p class="text hidden-lg">Win rate</p>
                                                <p class="text right">80%</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        </div>

    </div>



    <!-- JS library -->
    <?php include "html/js.html";?>
</body>

</html>