<!DOCTYPE html>
<html lang="en">
<head>
    <title>Fighting</title>
    <!-- head -->
    <?php include "html/head.html";?>
</head>
<body id="top" class="shop-page fighting-page back-ground-main-fighting">
<!-- Header -->
<?php include "html/header.html";?>

<!-- Main -->

<div class="figting">
<img src="image/figting/Sea_fighting.png" alt="" class="back-ground-main" />
    <seaction class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 col-12  figting_img">
                <img class="img_figting" src="image/figting/hunter-figting.png" alt="pitate">
                <div class="choose_pirates">
                <div class="block_info">
                        <div class="choose-pirates">
                            <img src="image/chractor/Icon_arrow_left.png" alt="" class="arrow-left">
                            <button type="button" class="btn text">
                                Choose your pirates
                            </button>
                            <img src="image/chractor/Icon_arrow_right.png" alt="" class="arrow-right">
                        </div>
                        <div class="info-name block_name">
                            <img src="image/My_pirates/Select/Infor/Layer_50.png" alt="" class="background-icon">
                            <img src="image/My_pirates/Select/Infor_4/legend.png" alt="" class="quality-icon">
                            <div class="name-block">
                                <p class="name">Connect wallet</p>
                                <p class="level">Level 12</p>
                            </div>
                        </div>
                        <div class="info">
                            <div class="fight">
                                <img src="image/figting/icon-power.png" alt="power">
                                <span>3456</span>
                            </div>
                            <div class="exp">
                                <img src="image/figting/icon-exp.png" alt="exp">
                                <span>3456</span>
                            </div>
                            <div class="fight">
                                <img src="image/figting/icon-energy.png" alt="energy">
                                <span>34566</span>
                            </div>
                        </div>
                    </div>
                    <img class="img_name" src="image/figting/fighting_pirate.png" alt="pitate">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-12 block_boss">
            <!-- <img class="back-ground-main hidden-md-up" src="image/figting/Sea_fighting.png" alt="pitate"> -->

                <div class="block_land one">
                    <div class="image_land">
                        <img src="image/figting/normal_land_new.png" alt="land">
                    </div>
                    <div class="normal_land">
                        <div class="boss_name">
                            <p class="name">Boss name</p>
                        </div>
                        <img src="image/figting/figh-figting.png" alt="figh figting">
                        <button type="button" class="fight_button"></button>
                    </div>
                </div>

                <div class="block_land two">
                    <div class="image_land">
                        <img src="image/figting/hard-land.png" alt="land">
                    </div>
                    <div class="normal_land">
                        <div class="boss_name">
                            <p class="name">Boss name</p>
                        </div>
                        <img src="image/figting/figh-figting.png" alt="figh figting">
                        <button type="button" class="fight_button"></button>
                    </div>
                </div>


                <div class="block_land three">
                    <div class="image_land">
                        <img src="image/figting/easy-land.png" alt="land">
                    </div>
                    <div class="normal_land">
                        <div class="boss_name">
                            <p class="name">Boss name</p>
                        </div>
                        <img src="image/figting/figh-figting.png" alt="figh figting">
                        <button type="button" class="fight_button"></button>
                    </div>
                </div>

                <div class="block_land four">
                    <div class="image_land">
                        <img src="image/figting/chanllenge-land.png" alt="land">
                    </div>
                    <div class="normal_land chanllenge_land_boss">
                        <div class="boss_name">
                            <p class="name">Boss name</p>
                        </div>
                        <img src="image/figting/figh-figting.png" alt="figh figting">
                        <button type="button" class="fight_button"></button>
                    </div>
                </div>
            </div>
        </div>
    </seaction>
</div>
<!-- JS library -->
<?php include "html/js.html";?>
</body>
</html>