<!DOCTYPE html>
<html lang="en">
<head>
    <title>Inventory</title>
    <!-- head -->
    <?php include "html/head.html";?>
</head>
<body id="top" class="shop-page-main">
<!-- Header -->
<?php include "html/header.html";?>

<div class="inventory-page">
    <img src="image/shop.png" alt="" class="back-ground-main" />
    <section class="container-fluid"></section>
</div>
<!-- JS library -->
<?php include "html/js.html";?>
</body>
</html>