<!DOCTYPE html>
<html lang="en">

<head>
    <title>Shop</title>
    <!-- head -->
    <?php include "html/head.html";?>
</head>

<body id="top" class="shop-page-main">
    <!-- Header -->
    <?php include "html/header.html";?>

    <div class="shop-page">
        <img src="image/shop.png" alt="" class="back-ground-main" />
        <section class="container-fluid">
            <div class="chest-box">
                <div class="island-image">
                    <img src="image/Shop/chest-shop.png" alt="" srcset="">
                    <div class="group-button">
                        <div class="price-block">
                            <img src="image/Shop/Header/coin.png" alt="" class="background-icon">
                            <p class="text">40000</p>
                        </div>
                        <div class="submit-block">
                            <img src="image/Buy.png" alt="" class="background-icon">
                            <button type="submit" class="btn btn-primary alert button-buy"></button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- JS library -->
    <?php include "html/js.html";?>
</body>
</html>