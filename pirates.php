<!DOCTYPE html>
<html lang="en">

<head>
    <title>My Pirates</title>
    <!-- head -->
    <?php include "html/head.html";?>
</head>

<body id="top" class="shop-page-main">
<!-- Header -->
<?php include "html/header.html";?>

<div class="shop-page pirates-page">
    <img src="image/shop.png" alt="" class="back-ground-main" />
    <section class="container-fluid">
        <div class="pirates_pages">
            <span class="scroll-down">
                    <img src="image/icon-down.png" alt="" class="fa-angle-double-down">
                </span>
            <div class="content-header">
                <div class="center">
                    <img src="image/steering-wheel.png" alt="" class="background-image-add">
                    <div class="title-popup">
                        <img src="image/Rope.png" alt="" class="background-image">
                        <p class="text">Pirates</p>
                    </div>
                </div>
                <div class="filter filter_dropdown">
                    <div class="filter_pirates filter_click">
                        <span>Filter</span>
                        <img src="image/Filter.png" alt="" class="box_filter">
                        <div class="icons_check">

                        </div>
                    </div>

                    <div class="filter_items">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"></a>
                        <div class="dropdown_filter">
                            <a href="#" class="dropdown_item">
                                Rare, Lowest to Highest
                            </a>
                            <a href="#" class="dropdown_item">
                                Rare, Highest to Lowest
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="listing">
                <table id="pirates" class="display">
                    <thead>
                    <tr class="hidden">
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="row">
                            <div class="pirates_list">

                                <div class="pirates-box">
                                    <div class="pirate_item">
                                        <img src="image/pirate-bg.png" alt="" class="item_bg">
                                        <div class="info_block_fighting">
                                            <div class="title_popup_item">
                                                <img src="image/top-img.png" alt="" class="item_bg_title">
                                                <h4 class="text">Barbu</h4>
                                            </div>
                                            <p class="number">111</p>
                                            <div class="fighting_item">
                                                <img src="image/fighting.png" alt="" class="">
                                            </div>
                                            <div class="armorial_item">
                                                <img src="image/armorial.png" alt="" class="item_bg">
                                                <span>Super rare</span>
                                            </div>
                                            <div class="info_bottom">
                                                <div class="info_item">
                                                    <img src="image/Group1.png" alt="power">
                                                    <span>Lever: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group2.png" alt="power">
                                                    <span>Power: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group3.png" alt="power">
                                                    <span>Exp: 4580</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group4.png" alt="power">
                                                    <span>Energy: 4580</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="pirates-box">
                                    <div class="pirate_item">
                                        <img src="image/pirate-bg.png" alt="" class="item_bg">
                                        <div class="info_block_fighting">
                                            <div class="title_popup_item">
                                                <img src="image/top-img.png" alt="" class="item_bg_title">
                                                <h4 class="text">Barbu</h4>
                                            </div>
                                            <p class="number">111</p>
                                            <div class="fighting_item">
                                                <img src="image/fighting.png" alt="" class="">
                                            </div>
                                            <div class="armorial_item">
                                                <img src="image/armorial.png" alt="" class="item_bg">
                                                <span>Super rare</span>
                                            </div>
                                            <div class="info_bottom">
                                                <div class="info_item">
                                                    <img src="image/Group1.png" alt="power">
                                                    <span>Lever: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group2.png" alt="power">
                                                    <span>Power: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group3.png" alt="power">
                                                    <span>Exp: 4580</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group4.png" alt="power">
                                                    <span>Energy: 4580</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="pirates-box">
                                    <div class="pirate_item">
                                        <img src="image/pirate-bg.png" alt="" class="item_bg">
                                        <div class="info_block_fighting">
                                            <div class="title_popup_item">
                                                <img src="image/top-img.png" alt="" class="item_bg_title">
                                                <h4 class="text">Barbu</h4>
                                            </div>
                                            <p class="number">111</p>
                                            <div class="fighting_item">
                                                <img src="image/fighting.png" alt="" class="">
                                            </div>
                                            <div class="armorial_item">
                                                <img src="image/armorial.png" alt="" class="item_bg">
                                                <span>Super rare</span>
                                            </div>
                                            <div class="info_bottom">
                                                <div class="info_item">
                                                    <img src="image/Group1.png" alt="power">
                                                    <span>Lever: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group2.png" alt="power">
                                                    <span>Power: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group3.png" alt="power">
                                                    <span>Exp: 4580</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group4.png" alt="power">
                                                    <span>Energy: 4580</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="pirates-box">
                                    <div class="pirate_item">
                                        <img src="image/pirate-bg.png" alt="" class="item_bg">
                                        <div class="info_block_fighting">
                                            <div class="title_popup_item">
                                                <img src="image/top-img.png" alt="" class="item_bg_title">
                                                <h4 class="text">Barbu</h4>
                                            </div>
                                            <p class="number">111</p>
                                            <div class="fighting_item">
                                                <img src="image/fighting.png" alt="" class="">
                                            </div>
                                            <div class="armorial_item">
                                                <img src="image/armorial.png" alt="" class="item_bg">
                                                <span>Super rare</span>
                                            </div>
                                            <div class="info_bottom">
                                                <div class="info_item">
                                                    <img src="image/Group1.png" alt="power">
                                                    <span>Lever: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group2.png" alt="power">
                                                    <span>Power: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group3.png" alt="power">
                                                    <span>Exp: 4580</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group4.png" alt="power">
                                                    <span>Energy: 4580</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <div class="pirates-box">
                                    <div class="pirate_item">
                                        <img src="image/pirate-bg.png" alt="" class="item_bg">
                                        <div class="info_block_fighting">
                                            <div class="title_popup_item">
                                                <img src="image/top-img.png" alt="" class="item_bg_title">
                                                <h4 class="text">Barbu</h4>
                                            </div>
                                            <p class="number">111</p>
                                            <div class="fighting_item">
                                                <img src="image/fighting.png" alt="" class="">
                                            </div>
                                            <div class="armorial_item">
                                                <img src="image/armorial.png" alt="" class="item_bg">
                                                <span>Super rare</span>
                                            </div>
                                            <div class="info_bottom">
                                                <div class="info_item">
                                                    <img src="image/Group1.png" alt="power">
                                                    <span>Lever: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group2.png" alt="power">
                                                    <span>Power: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group3.png" alt="power">
                                                    <span>Exp: 4580</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group4.png" alt="power">
                                                    <span>Energy: 4580</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="pirates-box">
                                    <div class="pirate_item">
                                        <img src="image/pirate-bg.png" alt="" class="item_bg">
                                        <div class="info_block_fighting">
                                            <div class="title_popup_item">
                                                <img src="image/top-img.png" alt="" class="item_bg_title">
                                                <h4 class="text">Barbu</h4>
                                            </div>
                                            <p class="number">111</p>
                                            <div class="fighting_item">
                                                <img src="image/fighting.png" alt="" class="">
                                            </div>
                                            <div class="armorial_item">
                                                <img src="image/armorial.png" alt="" class="item_bg">
                                                <span>Super rare</span>
                                            </div>
                                            <div class="info_bottom">
                                                <div class="info_item">
                                                    <img src="image/Group1.png" alt="power">
                                                    <span>Lever: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group2.png" alt="power">
                                                    <span>Power: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group3.png" alt="power">
                                                    <span>Exp: 4580</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group4.png" alt="power">
                                                    <span>Energy: 4580</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="pirates-box">
                                    <div class="pirate_item">
                                        <img src="image/pirate-bg.png" alt="" class="item_bg">
                                        <div class="info_block_fighting">
                                            <div class="title_popup_item">
                                                <img src="image/top-img.png" alt="" class="item_bg_title">
                                                <h4 class="text">Barbu</h4>
                                            </div>
                                            <p class="number">111</p>
                                            <div class="fighting_item">
                                                <img src="image/fighting.png" alt="" class="">
                                            </div>
                                            <div class="armorial_item">
                                                <img src="image/armorial.png" alt="" class="item_bg">
                                                <span>Super rare</span>
                                            </div>
                                            <div class="info_bottom">
                                                <div class="info_item">
                                                    <img src="image/Group1.png" alt="power">
                                                    <span>Lever: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group2.png" alt="power">
                                                    <span>Power: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group3.png" alt="power">
                                                    <span>Exp: 4580</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group4.png" alt="power">
                                                    <span>Energy: 4580</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="pirates-box">
                                    <div class="pirate_item">
                                        <img src="image/pirate-bg.png" alt="" class="item_bg">
                                        <div class="info_block_fighting">
                                            <div class="title_popup_item">
                                                <img src="image/top-img.png" alt="" class="item_bg_title">
                                                <h4 class="text">Barbu</h4>
                                            </div>
                                            <p class="number">111</p>
                                            <div class="fighting_item">
                                                <img src="image/fighting.png" alt="" class="">
                                            </div>
                                            <div class="armorial_item">
                                                <img src="image/armorial.png" alt="" class="item_bg">
                                                <span>Super rare</span>
                                            </div>
                                            <div class="info_bottom">
                                                <div class="info_item">
                                                    <img src="image/Group1.png" alt="power">
                                                    <span>Lever: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group2.png" alt="power">
                                                    <span>Power: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group3.png" alt="power">
                                                    <span>Exp: 4580</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group4.png" alt="power">
                                                    <span>Energy: 4580</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>


                                <div class="pirates-box">
                                    <div class="pirate_item">
                                        <img src="image/pirate-bg.png" alt="" class="item_bg">
                                        <div class="info_block_fighting">
                                            <div class="title_popup_item">
                                                <img src="image/top-img.png" alt="" class="item_bg_title">
                                                <h4 class="text">Barbu</h4>
                                            </div>
                                            <p class="number">111</p>
                                            <div class="fighting_item">
                                                <img src="image/fighting.png" alt="" class="">
                                            </div>
                                            <div class="armorial_item">
                                                <img src="image/armorial.png" alt="" class="item_bg">
                                                <span>Super rare</span>
                                            </div>
                                            <div class="info_bottom">
                                                <div class="info_item">
                                                    <img src="image/Group1.png" alt="power">
                                                    <span>Lever: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group2.png" alt="power">
                                                    <span>Power: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group3.png" alt="power">
                                                    <span>Exp: 4580</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group4.png" alt="power">
                                                    <span>Energy: 4580</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="pirates-box">
                                    <div class="pirate_item">
                                        <img src="image/pirate-bg.png" alt="" class="item_bg">
                                        <div class="info_block_fighting">
                                            <div class="title_popup_item">
                                                <img src="image/top-img.png" alt="" class="item_bg_title">
                                                <h4 class="text">Barbu</h4>
                                            </div>
                                            <p class="number">111</p>
                                            <div class="fighting_item">
                                                <img src="image/fighting.png" alt="" class="">
                                            </div>
                                            <div class="armorial_item">
                                                <img src="image/armorial.png" alt="" class="item_bg">
                                                <span>Super rare</span>
                                            </div>
                                            <div class="info_bottom">
                                                <div class="info_item">
                                                    <img src="image/Group1.png" alt="power">
                                                    <span>Lever: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group2.png" alt="power">
                                                    <span>Power: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group3.png" alt="power">
                                                    <span>Exp: 4580</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group4.png" alt="power">
                                                    <span>Energy: 4580</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="pirates-box">
                                    <div class="pirate_item">
                                        <img src="image/pirate-bg.png" alt="" class="item_bg">
                                        <div class="info_block_fighting">
                                            <div class="title_popup_item">
                                                <img src="image/top-img.png" alt="" class="item_bg_title">
                                                <h4 class="text">Barbu</h4>
                                            </div>
                                            <p class="number">111</p>
                                            <div class="fighting_item">
                                                <img src="image/fighting.png" alt="" class="">
                                            </div>
                                            <div class="armorial_item">
                                                <img src="image/armorial.png" alt="" class="item_bg">
                                                <span>Super rare</span>
                                            </div>
                                            <div class="info_bottom">
                                                <div class="info_item">
                                                    <img src="image/Group1.png" alt="power">
                                                    <span>Lever: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group2.png" alt="power">
                                                    <span>Power: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group3.png" alt="power">
                                                    <span>Exp: 4580</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group4.png" alt="power">
                                                    <span>Energy: 4580</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="pirates-box">
                                    <div class="pirate_item">
                                        <img src="image/pirate-bg.png" alt="" class="item_bg">
                                        <div class="info_block_fighting">
                                            <div class="title_popup_item">
                                                <img src="image/top-img.png" alt="" class="item_bg_title">
                                                <h4 class="text">Barbu</h4>
                                            </div>
                                            <p class="number">111</p>
                                            <div class="fighting_item">
                                                <img src="image/fighting.png" alt="" class="">
                                            </div>
                                            <div class="armorial_item">
                                                <img src="image/armorial.png" alt="" class="item_bg">
                                                <span>Super rare</span>
                                            </div>
                                            <div class="info_bottom">
                                                <div class="info_item">
                                                    <img src="image/Group1.png" alt="power">
                                                    <span>Lever: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group2.png" alt="power">
                                                    <span>Power: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group3.png" alt="power">
                                                    <span>Exp: 4580</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group4.png" alt="power">
                                                    <span>Energy: 4580</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>



                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="row">
                            <div class="pirates_list">
                                <div class="pirates-box">
                                    <div class="pirate_item">
                                        <img src="image/pirate-bg.png" alt="" class="item_bg">
                                        <div class="info_block_fighting">
                                            <div class="title_popup_item">
                                                <img src="image/top-img.png" alt="" class="item_bg_title">
                                                <h4 class="text">Barbu</h4>
                                            </div>
                                            <p class="number">111</p>
                                            <div class="fighting_item">
                                                <img src="image/fighting.png" alt="" class="">
                                            </div>
                                            <div class="armorial_item">
                                                <img src="image/armorial.png" alt="" class="item_bg">
                                                <span>Super rare</span>
                                            </div>
                                            <div class="info_bottom">
                                                <div class="info_item">
                                                    <img src="image/Group1.png" alt="power">
                                                    <span>Lever: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group2.png" alt="power">
                                                    <span>Power: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group3.png" alt="power">
                                                    <span>Exp: 4580</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group4.png" alt="power">
                                                    <span>Energy: 4580</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="pirates-box">
                                    <div class="pirate_item">
                                        <img src="image/pirate-bg.png" alt="" class="item_bg">
                                        <div class="info_block_fighting">
                                            <div class="title_popup_item">
                                                <img src="image/top-img.png" alt="" class="item_bg_title">
                                                <h4 class="text">Barbu</h4>
                                            </div>
                                            <p class="number">111</p>
                                            <div class="fighting_item">
                                                <img src="image/fighting.png" alt="" class="">
                                            </div>
                                            <div class="armorial_item">
                                                <img src="image/armorial.png" alt="" class="item_bg">
                                                <span>Super rare</span>
                                            </div>
                                            <div class="info_bottom">
                                                <div class="info_item">
                                                    <img src="image/Group1.png" alt="power">
                                                    <span>Lever: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group2.png" alt="power">
                                                    <span>Power: 12</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group3.png" alt="power">
                                                    <span>Exp: 4580</span>
                                                </div>
                                                <div class="info_item">
                                                    <img src="image/Group4.png" alt="power">
                                                    <span>Energy: 4580</span>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

<!-- JS library -->
<?php include "html/js.html";?>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.css">
<script>
    $(document).ready( function () {
        var tableSettings = {
            "processing": true,
            "info": false,
            "lengthChange": false,
            "searching": false,
            "pageLength": 1,
            "pagingType": "full_numbers",
        };
        $('#pirates').DataTable(tableSettings);

    } );
</script>
</body>
</html>