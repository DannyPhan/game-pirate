//
//  Pirates Object
//  _____________________________________________

var Pirates = {
    options: {
        popups: ['setting', 'claim', 'wallet', 'button-buy', 'choose_pirates', 'alert', , 'top-player', 'even', 'win-ability','treasure-hunted', 'rules'],
        dropdowns: ['dropdown', 'filter_dropdown']
    },

    /**
     * Init
     * @private
     */
    _init: function () {
        var $this = this;
        $(document).ready(function () {
            //Mobile menu
            $this.menuAction();

            //Dropdown
            $this.dropdownAction();

            //Dropdown Filter
            $this.dropdownFilter();

            //CountDown
            $this.countDown();

            //Bidding data
            $this._bindings();
        });
    },

    /**
     * Show popup
     */
    showPopup: function () {
        var $body = $('body');
        $(this.options.popups).each(function (index, cls) {
            $body.on('click', '.' + cls, function () {
                $body.addClass('popup-active');
                $('.' + cls + '-popup').addClass('active');
                $('.' + cls).addClass('active-click-popup');
            })
        });
    },

    /**
     * Close popup
     */
    closePopup: function () {
        $('.popup .close-modal').on('click', function (e) {
            $(this).closest('.popup').removeClass('active');
            $('body').removeClass('popup-active');
            if ($( "div" ).hasClass('active-click-popup')) {
                $( "div" ).removeClass( 'active-click-popup');
            } 
        })
    },

    /**
     * Menu action
     */
    menuAction: function () {
        $(document).on('click', function (e) {
            var opened = $('header button.navbar-toggler').hasClass('collapsed');
            if (opened === true) {
                $('header nav.container-fluid').addClass('not-active');
            } else {
                $('header nav.container-fluid').removeClass('not-active');
            }
        });
    },

    /**
     * Change number
     */
    changeCount: function () {
        $('.minus').click(function () {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });

        $('.plus').click(function () {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
    },

    /**
     * Dropdown action
     */
    dropdownAction: function () {
        $(this.options.dropdowns).each(function (index, cls) {
            $('.' + cls).hover(function () {
                var menu = $(this).children('.' + cls + '-menu');
                if (menu.is(":visible")) {
                    menu.parent().toggleClass("open");
                }
            })
        })
    },

    /**
     * Dropdown Filter
     */

    dropdownFilter: function () {
        $('.filter_click').on('click', function (e) {
            $(this).parent().toggleClass("open");
        })
    },

    /**
     * Tab action
     */
    tabAction: function () {
        $('#tabs-nav li').click(function () {
            $('#tabs-nav li').removeClass('active');
            $(this).addClass('active');
            $('.tab-content').removeClass("active-content");
            var activeTab = $(this).find('a').attr('href');
            $(activeTab).addClass('active-content');
            return false;
        });
    },


    /**
     * Countdown 
     */

     countDown: function(){
       if($('#clockdiv').length){
        function getTimeRemaining(endtime) {
          var t = Date.parse(endtime) - Date.parse(new Date());
          var seconds = Math.floor((t / 1000) % 60);
          var minutes = Math.floor((t / 1000 / 60) % 60);
          var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
          return {
            'total': t,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
          };
        }
        
        function initializeClock(id, endtime) {
    
          var clock = document.getElementById(id);
          var hoursSpan = clock.querySelector('.hours');
          var minutesSpan = clock.querySelector('.minutes');
          var secondsSpan = clock.querySelector('.seconds');
        
          function updateClock() {
            var t = getTimeRemaining(endtime);
        
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
        
            if (t.total <= 0) {
              clearInterval(timeinterval);
            }
          }
        
          updateClock();
          var timeinterval = setInterval(updateClock, 1000);
          }
          
        var deadline = new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000);
        initializeClock('clockdiv', deadline);
       }

    },

    /**
     * Binding data
     *
     * @private
     */
    _bindings: function () {
        //Show popup
        this.showPopup();

        // close popup
        this.closePopup();

        this.changeCount();
        this.tabAction();
    }
}
Pirates._init();

